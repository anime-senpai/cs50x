package edu.harvard.cs50.pokedex;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class PokemonActivity extends AppCompatActivity {
    private TextView nameTextView;
    private TextView numberTextView;
    private TextView type1TextView;
    private TextView type2TextView;
    private TextView descTextView;
    private String url;
    private RequestQueue requestQueue;
    private Button buttonCatch;
    private boolean caught;
    private String name_temp;
    private ImageView poke_img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokemon);

        requestQueue = Volley.newRequestQueue(getApplicationContext());
        url = getIntent().getStringExtra("url");
        name_temp = getIntent().getStringExtra("name");
        nameTextView = findViewById(R.id.pokemon_name);
        numberTextView = findViewById(R.id.pokemon_number);
        type1TextView = findViewById(R.id.pokemon_type1);
        type2TextView = findViewById(R.id.pokemon_type2);
        buttonCatch = findViewById(R.id.pokemon_catch);
        poke_img = findViewById(R.id.pokemon_img);
        descTextView = findViewById(R.id.pokemon_desc);

        load();

        //load map
        Map<String,Boolean> hashmap = loadMap();

        //fill caught
        if (hashmap.get(name_temp) != null) {
            caught = hashmap.get(name_temp);
        }else{
            caught = false;

            Log.e("Failed to load",name_temp);
        }

        if(caught){
            buttonCatch.setText("Release");
        }else{
            buttonCatch.setText("Catch");
        }
    }

    public void load_desc(Integer id){
        String url_desc = "https://pokeapi.co/api/v2/pokemon-species/" + id.toString();
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url_desc, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {

                    JSONArray descEntries = response.getJSONArray("flavor_text_entries");
                    JSONObject descEntry = descEntries.getJSONObject(0);
                    descTextView.setText(descEntry.getString("flavor_text"));

                } catch (JSONException e) {
                    Log.e("cs50", "Pokemon desc json error", e);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("cs50", "Pokemon desc details error", error);
            }
        });

        requestQueue.add(request);
    }

    public void load() {
        type1TextView.setText("");
        type2TextView.setText("");

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    nameTextView.setText(name_temp);
                    numberTextView.setText(String.format("#%03d", response.getInt("id")));

                    load_desc(response.getInt("id"));

                    JSONArray typeEntries = response.getJSONArray("types");
                    for (int i = 0; i < typeEntries.length(); i++) {
                        JSONObject typeEntry = typeEntries.getJSONObject(i);
                        int slot = typeEntry.getInt("slot");
                        String type = typeEntry.getJSONObject("type").getString("name");

                        if (slot == 1) {
                            type1TextView.setText(type);
                        }
                        else if (slot == 2) {
                            type2TextView.setText(type);
                        }
                    }

                    JSONObject sprites = response.getJSONObject("sprites");
                    String url_img = sprites.getString("front_default");
                    new DownloadSpriteTask().execute(url_img);


                } catch (JSONException e) {
                    Log.e("cs50", "Pokemon json error", e);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("cs50", "Pokemon details error", error);
            }
        });

        requestQueue.add(request);
    }

    public void toggleCatch(View view) {
        // gotta catch 'em all!

        Map<String,Boolean> caughtMap;
        //load map
        caughtMap = loadMap();

        if(caught){
            buttonCatch.setText("Catch");
            caughtMap.put(name_temp,false);
            //update caught
            caught = false;
        }else{
            buttonCatch.setText("Release");
            caughtMap.put(name_temp,true);
            //update caught
            caught = true;
        }

        //save map
        saveMap(caughtMap);
    }

    private void saveMap(Map<String,Boolean> inputMap){
        SharedPreferences pSharedPref = getApplicationContext().getSharedPreferences("MyVariables", Context.MODE_PRIVATE);
        if (pSharedPref != null){
            JSONObject jsonObject = new JSONObject(inputMap);
            String jsonString = jsonObject.toString();
            SharedPreferences.Editor editor = pSharedPref.edit();
            editor.remove("My_map").commit();
            editor.putString("My_map", jsonString);
            editor.commit();
        }
    }

    private Map<String,Boolean> loadMap(){
        Map<String,Boolean> outputMap = new HashMap<String,Boolean>();
        SharedPreferences pSharedPref = getApplicationContext().getSharedPreferences("MyVariables", Context.MODE_PRIVATE);
        try{
            if (pSharedPref != null){
                String jsonString = pSharedPref.getString("My_map", (new JSONObject()).toString());
                JSONObject jsonObject = new JSONObject(jsonString);
                Iterator<String> keysItr = jsonObject.keys();
                while(keysItr.hasNext()) {
                    String key = keysItr.next();
                    Boolean value = (Boolean) jsonObject.get(key);
                    outputMap.put(key, value);
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return outputMap;
    }

    private class DownloadSpriteTask extends AsyncTask<String, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... strings) {
            try {
                URL url = new URL(strings[0]);
                return BitmapFactory.decodeStream(url.openStream());
            }
            catch (IOException e) {
                Log.e("cs50", "Download sprite error", e);
                return null;
            }
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            // load the bitmap into the ImageView!
            poke_img.setImageBitmap(bitmap);
        }
    }
}
