package com.aea.kumpa;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import org.json.JSONObject;


import java.util.ArrayList;
import java.util.Arrays;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

public class MainActivity extends AppCompatActivity {

    private ImageView avatarImageView;
    private Bitmap avatarBitmap;
    private Button joinBtn;
    private Button createBtn;
    private Button change_avatar;
    private EditText nickInput;
    public static Socket mSocket;
    public static KumpaDatabase database;

    public static Integer avatar = 1;
    private Integer creator;
    public static  String nickname;
    private Integer room;
    public static Boolean avatar_changed;
    public static Boolean connected = false;
    public static Integer roomNumber;
    public static Integer width;

    //private Button testBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        creator = 1;
        nickname = "";
        roomNumber = -1;

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        width = metrics.widthPixels;

        database = Room
                .databaseBuilder(getApplicationContext(), KumpaDatabase.class, "kumpa")
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build();

        createDatabase();

        avatarImageView = findViewById(R.id.avatarImg);
        avatarImageView.setImageResource(getImageId(this, avatar));
        avatarBitmap = ((BitmapDrawable) avatarImageView.getDrawable()).getBitmap();
        //circleAvatar();
        Glide.with(this).load(avatarBitmap)
            .apply(RequestOptions.bitmapTransform(new CropCircleTransformation()))
            .into(avatarImageView);
        avatarImageView.requestLayout();

        nickInput = findViewById(R.id.nickInput);

        joinBtn = findViewById(R.id.join_btn);
        joinBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                Context context = v.getContext();
                Intent intent = new Intent(v.getContext(), JoinLobbyActivity.class);
                intent.putExtra("nickname", nickInput.getText().toString());
                intent.putExtra("creator", 0);
                intent.putExtra("avatar", avatar);
                context.startActivity(intent);
            }
        });

        createBtn = findViewById(R.id.create_btn);
        createBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button

                try{
                    nickname = nickInput.getText().toString();
                    creator = 1;

                    JSONObject jsonData = new JSONObject();
                    jsonData.put("avatar",avatar.toString());
                    jsonData.put("creator",creator.toString());
                    jsonData.put("nickname",nickname);
                    mSocket.emit("create_room", jsonData);
                }
                catch(Exception e){
                    Log.d("socket","error parsing JSON on create_room");
                }
            }
        });

        change_avatar = findViewById(R.id.change_avatar);
        change_avatar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                Context context = v.getContext();
                Intent intent = new Intent(context, AvatarActivity.class);
                context.startActivity(intent);
            }
        });

        /*testBtn = findViewById(R.id.test_btn);
        testBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                ArrayList<Player> players = new ArrayList<>(Arrays.asList(
                        new Player("F", 1, 1, 5),
                        new Player("Senpai", 2, 0, 7),
                        new Player("Machino", 3, 0, 2),
                        new Player("Haruchon", 4, 0, 1),
                        new Player("JorgexD", 5, 0, 3)
                        ));
                Context context = v.getContext();
                Intent intent = new Intent(context, ScoresActivity.class);
                intent.putExtra("room", 1);
                intent.putExtra("avatar", avatar);
                intent.putExtra("creator", creator);
                intent.putExtra("nickname", nickname);
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList("players", players);
                intent.putExtras(bundle);
                context.startActivity(intent);
            }
        });*/

        if(!connected){
            connected = true;
            try {
                IO.Options options = new IO.Options();
                options.reconnection = true;
                options.timeout = 2000000;
                mSocket = IO.socket("http://18.212.173.93:3000/",options);
                mSocket.on(Socket.EVENT_CONNECT, new Emitter.Listener(){
                    @Override
                    public void call(Object... args) {
                        try{

                            JSONObject jsonData = new JSONObject();
                            jsonData.put("room", roomNumber);
                            jsonData.put("nickname", nickname);
                            mSocket.emit("rejoin_room", jsonData);
                        }
                        catch(Exception e){
                            Log.d("socket","error parsing JSON on create_room");
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                            }
                        });
                    }
                });

                mSocket.on(Socket.EVENT_DISCONNECT, new Emitter.Listener(){
                    @Override
                    public void call(Object... args) {
                        try{
                            mSocket.connect();
                        }
                        catch(Exception e){
                            Log.d("socket","error parsing JSON on create_room");
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                            }
                        });
                    }
                });

                roomCreatedListener();
                mSocket.connect();

            } catch (Exception e) {
                Log.d("socket","error connecting with socket");
            }
        }

    }

    private void roomCreatedListener() {
        mSocket.on("room_created", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                room = Integer.parseInt(args[0].toString());
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                    Context context = getApplicationContext();
                    Intent intent = new Intent(context, LobbyActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("nickname", nickname);
                    intent.putExtra("creator", creator);
                    intent.putExtra("avatar", avatar);
                    intent.putExtra("room", room);
                    intent.putExtra("firstGame", true);
                    context.startActivity(intent);
                    }
                });
            }
        });
    }

    /*private void databaseCreation(){
        SharedPreferences pSharedPref = getApplicationContext().getSharedPreferences("kumpaVariables2", Context.MODE_PRIVATE);
        if (pSharedPref != null){
            Boolean initial_commit = pSharedPref.getBoolean("initial_commit", true);

            //if it's the first time
            if(initial_commit){
                //call create
                database.questionDao().create();

                //update initial_commit to false
                SharedPreferences.Editor editor = pSharedPref.edit();
                editor.remove("initial_commit").commit();
                editor.putBoolean("initial_commit", false);
                editor.commit();
            }
        }
    }*/

    public static int getImageId(Context context, Integer imageName) {
        return context.getResources().getIdentifier("drawable/a" + imageName.toString(), null, context.getPackageName());
    }

    private void createDatabase(){
        Integer n = database.questionDao().count();

        ArrayList<Question> listQ = new ArrayList<>(Arrays.asList(
                new Question(1, "_nick_ would say the best Christmas movie is", "Home Alone", "Its a Wonderful Life", "Christmas Vacation", "Die Hard"),
                new Question(2, "When _nick_ engages in small talk, its usually about:", "Weather", "Sports", "Politics", "Themself"),
                new Question(3, "What would _nick_ like to experience?", "World peace", "Encounter of aliens", "Technology to talk to animals", "A zombie invasion"),
                new Question(4, "_nick_ would like to have now a ...", "Full phone battery", "Bigger screen", "Better company", "Stable internet connection"),
                new Question(5, "What is _nick_'s favorite season of the year?", "Winter", "Spring", "Summer", "Autumn"),
                new Question(6, "What is _nick_'s most commonly occuring nightmare?", "Losing theeir teeth", "Public nudity", "Paranormal experience", "Being attacked or chased"),
                new Question(7, "If _nick_ gets invited to an event, _nick_ replies:", "Yes", "No", "I have a funeral", "Maybe"),
                new Question(8, "What is _nick_ favourite kind of sandwich?", "Egg", "Peanut butter", "Hot dog", "Cheese burger"),
                new Question(9, "Who is _nick_'s favourite Stephen?", "Stephen Hawking", "Stephen King", "Stephen Colbert", "Stephen Spielberg"),
                new Question(10, "Who is _nick_'s favourite female avenger?", "Black Widow", "Scarlet Witch", "Captain Marvel", "Shuri"),
                new Question(11, "Has _nick_ ever friendzoned someone?", "Yes", "No", "It's my hobby", "I have lost a lot of friends"),
                new Question(12, "Has _nick_ ever been stuck in the friendzone?", "Yes", "No", "I lost my best friend", "Maybe"),
                new Question(13, "Who would _nick_'s vote for if electable for president?", "Lelouch Vi Britannia", "Kirigaya Kazuto", "Okabe Rintarou", "Luffy"),
                new Question(14, "What is _nick_'s favourite movie genre?", "Super Heroes", "Romcom", "Horror", "Mistery"),
                new Question(15, "What has _nick_ been doing in quarantine?", "Studying", "Sleeping", "Watching series", "Playing games"),
                new Question(16, "_nick_ would not want to be famous because of ...?", "Stalkers", "Paparazzis", "Being a role model", "False friendships"),
                new Question(17, "_nick_'s favorite subject in school was ...?", "Art", "Math", "Sports", "Language"),
                new Question(18, "In which of these comedy series would _nick_ rather want to live?", "Friends", "HIMYM", "Suits", "Big Bang Theory"),
                new Question(19, "What of those things does _nick_ annoy most in movies?", "Unreal physics", "Run into the wrong direction", "Not picking up weapons", "The bad guys always lose"),
                new Question(20, "How many pairs of shoes does _nick_ own?", "Less than 5", "5-10", "10-20", "more than 20"),
                new Question(21, "Which time was the best for _nick_?", "Kindergarden", "School", "University", "Work life"),
                new Question(22, "Which is _nick_'s favourite Nickelodeon series?", "Big Time Rush", "Zoey 101", "iCarly", "Drake & Josh"),
                new Question(23, "_nick_'s favourite side dish is ...?", "Potatos", "Rice", "Pasta", "Salad"),
                new Question(24, "Which is _nick_'s favourite Disney channel series?", "Wizards of Waverly Place", "Good Luck Charlie", "Austin & Ally", "Jessie"),
                new Question(25, "Which is _nick_'s favourite Nickelodeon cartoon?", "The Fairly OddParents", "Jimmy Neutron", "Sponge Bob", "Hey Arnold!"),
                new Question(26, "Which is _nick_'s favourite CN cartoon?", "The Grim Adventures of Billy & Mandy", "KND", "Foster´s Home for Imaginary Friends", "Ben 10"),
                new Question(27, "_nick_'s most ticklish spot is?", "Feet", "Belly", "Neck", "Not ticklish"),
                new Question(28, "How does _nick_ prepare for a test?", "Not at all", "Studying weeks before", "Studying the whole night before", "Cheats in the test"),
                new Question(29, "First thing _nick_ does in the morning?", "Shower", "Breakfast", "Check phone", "Snooze"),
                new Question(30, "_nick_ would team up with which villain?", "The Joker", "Lord Voldemort", "Darth Vader", "Thanos"),
                new Question(31, "Worst pain _nick_ ever experienced?", "Broken bone", "Paper cut", "Broken heart", "Wasp sting"),
                new Question(32, "_nick_ would like to live in a world of ...?", "Pirates", "Zombies", "Ninjas", "Vampires"),
                new Question(33, "If _nick_ was in a shoujo anime which role would (s)he play?", "Main character (MC)", "MC best friend", "Loser on love triangle", "Extra"),
                new Question(34, "If _nick_ was in a shonen anime which role would (s)he play?", "Main character (MC)", "MC teacher", "MC love interest", "Villain"),
                new Question(35, "What was the farthest _nick_ has been?", "On another continent", "Out of town", "Out of state", "Out of country"),
                new Question(36, "_nick_ would like to learn how to ...?", "Code", "Play an instrument", "Speak more languages", "Cook"),
                new Question(37, "How many languages does _nick_ speak?", "One", "Two", "Three or Four", "More than 5"),
                new Question(38, "How many kids does _nick_ want to have?", "One", "Two", "Three or more", "None"),
                new Question(39, "What is _nick_'s favourite snack?", "Cookies", "Potato chips", "Veggies", "Candy"),
                new Question(40, "_nick_ would rather play a major part in ...", "Game of thrones", "Breaking Bad", "Money Heist", "Attack on Titan"),
                new Question(41, "What is _nick_'s favourite sport?", "Soccer", "Basketball", "Volleyball", "Baseball"),
                new Question(42, "If you could check _nick_'s browser history, you'd find out:", "has been stalking someone", "has deleted their browser history", "has been researching", "A lot of puppies photos and videos"),
                new Question(43, "For 500$, _nick_'s would run down a busy street ...?", "Naked", "In underwear", "Topless", "Wouldn't do any of that"),
                new Question(44, "_nick_ is bad at?", "Flirting", "Giving compliments", "Being funny", "Dressing well"),
                new Question(45, "Who is _nick_'s favourite Disney Princess?", "Jasmine(Aladdin)", "Ariel(The Little Mermaid)", "Rapunzel(Tangled)", "Elsa(Fronzen)"),
                new Question(46, "Is _nick_'s crush in this room?", "Yes (Now is your chance)", "No", "Maybe (coward)", "Don't have a crush"),
                new Question(47, "What kind of person is _nick_?", "Cat", "Dog", "No pets", "Bird"),
                new Question(48, "What does _nick_ do while watching a movie?", "Fall asleep", "Ask questions", "Be on the phone", "Silence"),
                new Question(49, "_nick_ would not date a person who ...?", "Is super ugly", "Taked hard drugs", "Is radically religious", "Is arrogant and selfish"),
                new Question(50, "What type of games does _nick_ prefer?", "Single player", "Co-op", "Multiplayer online", "Board games"),
                new Question(51, "For a women magazine, _nick_ would write about ...?", "Sex", "Fitness tips", "Celebrities", "Men and what they really want"),
                new Question(52, "_nick_ would rather believe that ...?", "Iluminatu exists", "Governments know about aliens", "JFK was murderes by the CIA", "The moon landing in 1969 was fake"),
                new Question(53, "To wake up _nick_ needs ...?", "Drink coffee", "Excercise", "Cold shower", "Energy drink"),
                new Question(54, "Is _nick_ currently in a relationship?", "Yes", "No", "Several, actually", "Their situation is complicated"),
                new Question(55, "Which team _nick_ Griff join to attack the others ...?", "Batman & Optimus Prime", "Deadpool & Terminator", "Darth Vader & Wolverine", "Iron Man & Predator"),
                new Question(56, "Which technology is _nick_ awaiting the most eagerly?", "Nerve Gear", "Cloning", "Universal food generator", "Reliable working printers"),
                new Question(57, "What does _nick_ sleep in?", "Pijamas", "Naked", "Underwear", "An oversized shirt"),
                new Question(58, "When meeting people, _nick_ always comes ...", "Exactly on time", "A bit early", "A bit late", "Really late"),
                new Question(59, "What's a perfect first date for _nick_?", "Dinner and a movie", "Zoo visit", "Arcade visit", "Bike tour"),
                new Question(60, "What is _nick_ most scared of?", "Death", "Spiders", "Public speaking", "Lonelyness")
        ));

        Integer cant = listQ.size();

        if (n<20){
            for (int i = 0; i < cant; i++){
                Question q = listQ.get(i);
                database.questionDao().insert(q);
            }
        }
    }
}
