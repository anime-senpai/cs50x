package com.aea.kumpa;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {Question.class}, version = 2, exportSchema = false)
public abstract class KumpaDatabase extends RoomDatabase {
    public abstract QuestionDao questionDao();
}
