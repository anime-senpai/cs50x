package com.aea.kumpa;


import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AvatarAdapter extends RecyclerView.Adapter<AvatarAdapter.AvatarViewHolder> {

    public class AvatarViewHolder extends RecyclerView.ViewHolder{
        public ConstraintLayout containerView;
        public ImageView choose_avatar;

        public AvatarViewHolder(View view) {
            super(view);
            this.containerView = view.findViewById(R.id.avatar_element);
            this.choose_avatar = view.findViewById(R.id.choose_avatar);

            this.containerView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AvatarActivity.avatar_selected = (Integer) containerView.getTag();
                    notifyDataSetChanged();
                }
            });
        }

    }

    private List<Integer> avatars = new ArrayList<>(Arrays.asList(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15));

    @Override
    public AvatarAdapter.AvatarViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.avatar_element, parent, false);

        return new AvatarAdapter.AvatarViewHolder(view);
    }

    public static int getImageId(Context context, Integer imageName) {
        return context.getResources().getIdentifier("drawable/a" + imageName.toString(), null, context.getPackageName());
    }

    @Override
    public void onBindViewHolder(AvatarAdapter.AvatarViewHolder holder, int position) {
        Integer current = avatars.get(position);
        holder.containerView.setTag(current);
        if(current == AvatarActivity.avatar_selected){
            holder.containerView.setBackgroundResource(R.drawable.light_blue_background);
        }else{
            holder.containerView.setBackgroundColor(Color.TRANSPARENT);
        }
        holder.choose_avatar.setImageResource(getImageId(holder.containerView.getContext(), current));
    }

    @Override
    public int getItemCount() {
        return avatars.size();
    }

}
