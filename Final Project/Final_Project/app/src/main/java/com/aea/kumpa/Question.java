package com.aea.kumpa;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "questions")
public class Question {
    @PrimaryKey
    private int id;

    @ColumnInfo(name = "question_text")
    private String question_text;

    @ColumnInfo(name = "answer1")
    private String answer1;

    @ColumnInfo(name = "answer2")
    private String answer2;

    @ColumnInfo(name = "answer3")
    private String answer3;

    @ColumnInfo(name = "answer4")
    private String answer4;

    @ColumnInfo(name = "n_times")
    private int n_times;

    @ColumnInfo(name = "used")
    private int used;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQuestion_text() {
        return question_text;
    }

    public void setQuestion_text(String question_text) {
        this.question_text = question_text;
    }

    public String getAnswer1() {
        return answer1;
    }

    public void setAnswer1(String answer1) {
        this.answer1 = answer1;
    }

    public String getAnswer2() {
        return answer2;
    }

    public void setAnswer2(String answer2) {
        this.answer2 = answer2;
    }

    public String getAnswer3() {
        return answer3;
    }

    public void setAnswer3(String answer3) {
        this.answer3 = answer3;
    }

    public String getAnswer4() {
        return answer4;
    }

    public void setAnswer4(String answer4) {
        this.answer4 = answer4;
    }

    public int getN_times() {
        return n_times;
    }

    public void setN_times(int n_times) {
        this.n_times = n_times;
    }

    Question(){
    }

    Question(int id, String question_text, String answer1, String answer2, String answer3, String answer4){
        this.id = id;
        this.question_text = question_text;
        this.answer1 = answer1;
        this.answer2 = answer2;
        this.answer3 = answer3;
        this.answer4 = answer4;
        this.n_times = 0;
        this.used = 0;
    }

    public int getUsed() {
        return used;
    }

    public void setUsed(int used) {
        this.used = used;
    }
}
