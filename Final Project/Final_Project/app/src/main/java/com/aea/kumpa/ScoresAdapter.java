package com.aea.kumpa;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ScoresAdapter extends RecyclerView.Adapter<ScoresAdapter.ScoresViewHolder> {

    public class ScoresViewHolder extends RecyclerView.ViewHolder{
        public LinearLayout containerView;
        public ImageView scoreAvatarImg;
        public TextView scoreNickText;
        public TextView playerScore;
        public TextView scoreBackground;

        public ScoresViewHolder(View view) {
            super(view);
            this.containerView = view.findViewById(R.id.score_row);
            this.scoreAvatarImg = view.findViewById(R.id.scoreAvatarImg);
            this.scoreNickText = view.findViewById(R.id.score_nickname);
            this.scoreBackground = view.findViewById(R.id.score_background);
            this.playerScore = view.findViewById(R.id.playerScore);

        }


    }

    private List<Player> scores = new ArrayList<>();
    private List<Integer> backgrounds = new ArrayList<>(Arrays.asList(
            R.drawable.golden_background,
            R.drawable.silver_background,
            R.drawable.bronce_background,
            R.drawable.light_blue_background
    ));

    @Override
    public ScoresAdapter.ScoresViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.score_row, parent, false);

        return new ScoresAdapter.ScoresViewHolder(view);
    }

    public static int getImageId(Context context, Integer imageName) {
        return context.getResources().getIdentifier("drawable/a" + imageName.toString(), null, context.getPackageName());
    }

    @Override
    public void onBindViewHolder(ScoresAdapter.ScoresViewHolder holder, int position) {
        Player current = scores.get(position);
        holder.containerView.setTag(current);

        int n = (200 + 100 * current.getScore() / ScoresActivity.maxScore) * 3;
        if (n > MainActivity.width){
            n = n * 2 / 3;
        }
        int pos = current.getPlace();
        if(pos>=3)
            pos = 3;

        ViewGroup.LayoutParams params = holder.scoreBackground.getLayoutParams();
        params.width = n;
        holder.scoreBackground.setLayoutParams(params);
        holder.scoreBackground.setBackgroundResource(backgrounds.get(pos));

        holder.scoreAvatarImg.setImageResource(getImageId(holder.containerView.getContext(), current.getAvatar()));
        Integer sc = current.getScore();
        holder.scoreNickText.setText(current.getNickname());
        holder.playerScore.setText(sc.toString());
    }

    @Override
    public int getItemCount() {
        return scores.size();
    }

    /*public List<Player> getData() {
        notifyDataSetChanged();
        return scores;
    }*/

    public void loadData(List<Player> players) {
        int n = players.size();
        for (int i = 0; i < n; i++){
            Player p1 = players.get(i);
            p1.setPlace(i);
            scores.add(p1);
        }
        notifyDataSetChanged();
    }

    /*public void addPlayer(Player p) {
        scores.add(p);
        notifyDataSetChanged();
        return;
    }*/
}
