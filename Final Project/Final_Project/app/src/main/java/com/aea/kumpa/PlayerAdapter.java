package com.aea.kumpa;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PlayerAdapter extends RecyclerView.Adapter<PlayerAdapter.PlayerViewHolder> {
    public class PlayerViewHolder extends RecyclerView.ViewHolder{
        public LinearLayout containerView;
        public ImageView playerAvatarImg;
        public TextView playerNickText;
        public ImageButton exitBtn;
        public ImageButton readyBtn;

        public PlayerViewHolder(View view) {
            super(view);
            this.containerView = view.findViewById(R.id.player_row);
            this.playerAvatarImg = view.findViewById(R.id.playerAvatarImg);
            this.playerNickText = view.findViewById(R.id.playerNickText);
            this.exitBtn = view.findViewById(R.id.exitBtn);
            this.readyBtn = view.findViewById(R.id.readyBtn);

            exitBtn.setVisibility(View.INVISIBLE);
            readyBtn.setVisibility(View.INVISIBLE);

            /*this.exitBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    removeAt(getAdapterPosition());
                }
            });*/


        }


    }

    private List<Player> players = new ArrayList<>();

    @Override
    public PlayerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.player_row, parent, false);

        return new PlayerViewHolder(view);
    }

    public static int getImageId(Context context, Integer imageName) {
        return context.getResources().getIdentifier("drawable/a" + imageName.toString(), null, context.getPackageName());
    }

    @Override
    public void onBindViewHolder(PlayerViewHolder holder, int position) {
        Player current = players.get(position);
        holder.containerView.setTag(current);

        //holder.playerAvatarImg.setImageResource(R.drawable.a1);
        holder.playerAvatarImg.setImageResource(getImageId(holder.containerView.getContext(), current.getAvatar()));
        holder.playerNickText.setText(current.getNickname());
    }

    @Override
    public int getItemCount() {
        return players.size();
    }

    public void resetAdapter(){
        players.clear();
    }

    public void removeAt(int position) {
        players.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, players.size());
    }

    public void removePlayer(Player p){
        int n = players.size();
        for (int i = 0; i < n; i++){
            if (p.getNickname().equals(players.get(i).getNickname())){
                removeAt(i);
                return;
            }
        }
    }

    public List<Player> getData() {
        notifyDataSetChanged();
        return players;
    }
    public void addPlayer(Player p) {
        players.add(p);
        notifyDataSetChanged();
        return;
    }
}
