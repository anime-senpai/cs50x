package com.aea.kumpa;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.RunnableFuture;

import io.socket.emitter.Emitter;

public class ScoresActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private ScoresAdapter adapter;
    private TextView scoresText;
    private Button play_again_btn;
    private Button main_menu_btn;

    private ArrayList<Player> players;
    private Player user;
    private Integer roomNumber;

    public static Integer maxScore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scores);

        Bundle bundle = getIntent().getExtras();
        players = bundle.getParcelableArrayList("players");

        Intent intent = getIntent();
        Integer avatar = intent.getIntExtra("avatar", 0);
        Integer creator = intent.getIntExtra("creator", 0);
        String nickname = intent.getStringExtra("nickname");
        user = new Player(nickname, avatar, creator);
        roomNumber = intent.getIntExtra("room", 0);

        sortPlayers();

        maxScore = players.get(0).getScore();
        if(maxScore < 1)
            maxScore = 1;

        scoresText = findViewById(R.id.scoresText);
        String s1 = players.get(0).getNickname() + " Wins";
        scoresText.setText(s1);

        System.out.println(maxScore);

        recyclerView = findViewById(R.id.scores_recycler_view);
        layoutManager = new LinearLayoutManager(this);
        adapter = new ScoresAdapter();
        adapter.loadData(players);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        play_again_btn = findViewById(R.id.play_again_btn);
        play_again_btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                Context context = getApplicationContext();
                Intent intent = new Intent(context, LobbyActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("nickname", user.getNickname());
                intent.putExtra("creator", user.getCreator());
                intent.putExtra("avatar", user.getAvatar());
                intent.putExtra("room", roomNumber);
                intent.putExtra("firstGame", false);
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList("players", players);
                intent.putExtras(bundle);
                context.startActivity(intent);
            }
        });



        main_menu_btn = findViewById(R.id.main_menu_btn);
        main_menu_btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                //emit leave_room

                try {
                    JSONObject jsonData = new JSONObject();
                    jsonData.put("room", roomNumber);
                    jsonData.put("nickname", user.getNickname());
                    MainActivity.mSocket.emit("leave_room", jsonData);
                    MainActivity.roomNumber = -1;
                } catch (Exception e){
                    Log.d("socket","error calling leave_room");
                }

                Context context = v.getContext();
                Intent intent = new Intent(context, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(intent);
            }
        });

        removePlayerListener();
    }

    private void sortPlayers(){
        Collections.sort(players, new Comparator<Player>() {
            @Override
            public int compare(Player p1, Player p2) {
                if (p1.getScore() < p2.getScore())
                    return 1;
                if (p1.getScore() > p2.getScore())
                    return -1;
                return 0;
            }
        });
    }

    private void removePlayerListener() {
        MainActivity.mSocket.on("remove_player", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.d("socket","remove_player reached");
                JSONObject playerJson = null;
                Player p = null;
                try {
                    playerJson = new JSONObject(args[0].toString());
                    String nickname = playerJson.getString("nickname");
                    p = new Player(nickname,1,0);
                    removePlayer(p.getNickname());

                } catch (JSONException e) {
                    Log.d("socket","error parsing Json on new_player");
                    e.printStackTrace();
                }
            }
        });
    }

    private void removePlayer(String nick){
        int n = players.size();
        for (int i = 0; i < n; i++){
            if (nick.equals(players.get(i).getNickname())){
                players.remove(i);
                return;
            }
        }
    }

    @Override
    public void onBackPressed() {

    }
}