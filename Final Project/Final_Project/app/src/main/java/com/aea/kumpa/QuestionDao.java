package com.aea.kumpa;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface QuestionDao {

    @Insert
    void insert(Question question);

    @Query("SELECT COUNT(*) FROM questions")
    Integer count();

    @Query("DELETE FROM questions")
    void delete();

    @Query("SELECT * FROM questions")
    List<Question> getAll();

    @Query("SELECT * FROM questions WHERE used = 0 AND n_times = (SELECT MIN(n_times) FROM questions) ORDER BY abs(random()) DESC LIMIT 1")
    Question chooseQuestion();

    @Query("UPDATE questions SET n_times = :n_times, used = :used WHERE id = :id")
    void update(int n_times, int used, int id);

    @Query("SELECT * FROM questions WHERE id = :id LIMIT 1")
    Question getById(int id);

    @Query("UPDATE questions SET  used = 0")
    void endGame();
}
