package com.aea.kumpa;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class AvatarActivity extends AppCompatActivity {

    private Button save;
    private RecyclerView recyclerView;
    private GridLayoutManager layoutManager;
    private AvatarAdapter adapter;

    public static Integer avatar_selected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_avatar);

        avatar_selected = MainActivity.avatar;

        recyclerView = findViewById(R.id.avatars_recycler_view);
        layoutManager = new GridLayoutManager(this,3);
        adapter = new AvatarAdapter();

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        save = findViewById(R.id.save_avatar);
        save.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                MainActivity.avatar = avatar_selected;
                Context context = v.getContext();
                Intent intent = new Intent(context, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(intent);
            }
        });
    }
}