package com.aea.kumpa;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.RunnableFuture;

import io.socket.emitter.Emitter;

public class LobbyActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private PlayerAdapter adapter;
    private TextView roomText;
    private Button startBtn;
    private EditText roundsInput;
    private TextView roundsText;
    private Integer roomNumber;
    private Player user;
    public static Player p;
    private ArrayList<Player> players;
    private Integer numRounds = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lobby);

        Intent intent = getIntent();
        Integer avatar = intent.getIntExtra("avatar", 0);
        Integer creator = intent.getIntExtra("creator", 0);
        String nickname = intent.getStringExtra("nickname");
        roomNumber = intent.getIntExtra("room", 0);
        System.out.println("Room: " + roomNumber.toString());
        user = new Player(nickname,avatar,creator);

        MainActivity.roomNumber = roomNumber;
        MainActivity.nickname = nickname;


        roundsInput = findViewById(R.id.roundsInput);
        roundsText = findViewById(R.id.roundsText);

        if(user.getCreator() == 0){
            roundsInput.setVisibility(View.INVISIBLE);
            roundsText.setVisibility(View.INVISIBLE);
        }

        roomText = findViewById(R.id.roomText);
        String sTemp = "Room N° " + roomNumber.toString();
        roomText.setText(sTemp);

        recyclerView = findViewById(R.id.players_recycler_view);
        layoutManager = new LinearLayoutManager(this);
        adapter = new PlayerAdapter();

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        Boolean b = intent.getBooleanExtra("firstGame",true);
        if (!b){
            Bundle bundle = getIntent().getExtras();
            players = bundle.getParcelableArrayList("players");
            adapter.resetAdapter();
            int n = players.size();
            for (int i = 0; i < n; i++){
                Player p1 = players.get(i);
                p1.setScore(0);
                adapter.addPlayer(p1);
            }
        } else{
            joinRoom();
        }

        startBtn = findViewById(R.id.startBtn);

        startBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startGame();
            }
        });

        newPlayerListener();
        gameStartListener();
        removePlayerListener();

        errorConnJoinListener();
        errorConnStartListener();
        errorConnLeaveListener();
    }


    private void errorConnJoinListener() {
        MainActivity.mSocket.on("connection_error_join", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.d("socket","connection_error_join reached");
                joinRoom();
            }
        });
    }

    private void joinRoom(){
        try {
            JSONObject jsonData = new JSONObject();
            jsonData.put("avatar", user.getAvatar());
            jsonData.put("creator", user.getCreator());
            jsonData.put("nickname", user.getNickname());
            jsonData.put("room", roomNumber);
            MainActivity.mSocket.emit("join_room", jsonData);
        } catch (Exception e){
            Log.d("socket","error calling join room");
        }
    }



    private void errorConnStartListener() {
        MainActivity.mSocket.on("connection_error_start", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.d("socket","connection_error_start reached");
                startGame();
            }
        });
    }

    private void startGame(){
        if(user.getCreator() == 1){
            try {
                if((!roundsInput.getText().toString().matches("^[1-9]\\d*$"))){

                    Context context = getApplicationContext();
                    CharSequence text = "Rounds must be a number";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                    return;
                }
                numRounds = Integer.parseInt(roundsInput.getText().toString());
                JSONObject jsonData = new JSONObject();
                jsonData.put("room", roomNumber);
                jsonData.put("numRounds", numRounds);
                MainActivity.mSocket.emit("start_game", jsonData);
            } catch (Exception e){
                Log.d("socket","error calling join room");
            }
        }else{
            Context context = getApplicationContext();
            CharSequence text = "Wait for your host to begin the game";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        }
    }

    private void gameStartListener(){
        MainActivity.mSocket.on("game_started", new Emitter.Listener() {
            @Override
            public void call(Object... args) {

                Log.d("socket","game_started reached");
                JSONObject gameJson = null;
                p = null;
                try {
                    gameJson = new JSONObject(args[0].toString());
                    numRounds = gameJson.getInt("numRounds");

                } catch (JSONException e) {
                    Log.d("socket","error parsing Json on game_started");
                    e.printStackTrace();
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        List<Player> list =  adapter.getData();
                        // Code here executes on main thread after user presses button
                        Context context = getApplicationContext();
                        Intent intent = new Intent(context, QuestionActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("room", roomNumber);
                        intent.putExtra("avatar", user.getAvatar());
                        intent.putExtra("creator", user.getCreator());
                        intent.putExtra("nickname", user.getNickname());
                        intent.putExtra("numRounds", numRounds);
                        Bundle bundle = new Bundle();
                        Integer n = list.size();
                        System.out.println("players size " + n.toString());
                        bundle.putParcelableArrayList("players", (ArrayList<? extends Parcelable>) list);
                        intent.putExtras(bundle);
                        //intent.putExtra("players", (Serializable) list);
                        context.startActivity(intent);
                    }
                });
                //JSONObject messageJson = new JSONObject(args[0].toString());
                //String room = messageJson.getString("message");
            }
        });
    }

    private void newPlayerListener() {
        MainActivity.mSocket.on("new_player", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.d("socket","new_player reached");
                JSONObject playerJson = null;
                p = null;
                try {
                    playerJson = new JSONObject(args[0].toString());
                    String nickname = playerJson.getString("nickname");
                    Integer avatar = playerJson.getInt("avatar");
                    Integer creator = playerJson.getInt("creator");
                    p = new Player(nickname,avatar,creator);

                } catch (JSONException e) {
                    Log.d("socket","error parsing Json on new_player");
                    e.printStackTrace();
                }
                AddRunnable runnable = new AddRunnable();
                RunnableFuture<Void> task = new FutureTask<>(runnable, null);
                runOnUiThread(task);
                try {
                    task.get(); // this will block until Runnable completes
                } catch (InterruptedException | ExecutionException e) {
                    // handle exception
                    Log.d("socket","error handling threads on new_player");
                }
            }
        });
    }

    private void removePlayerListener() {
        MainActivity.mSocket.on("remove_player", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.d("socket","remove_player reached");
                JSONObject playerJson = null;
                p = null;
                try {
                    playerJson = new JSONObject(args[0].toString());
                    String nickname = playerJson.getString("nickname");
                    p = new Player(nickname,1,0);

                } catch (JSONException e) {
                    Log.d("socket","error parsing Json on new_player");
                    e.printStackTrace();
                }
                RemoveRunnable runnable = new RemoveRunnable();
                RunnableFuture<Void> task = new FutureTask<>(runnable, null);
                runOnUiThread(task);
                try {
                    task.get(); // this will block until Runnable completes
                } catch (InterruptedException | ExecutionException e) {
                    // handle exception
                    Log.d("socket","error handling threads on new_player");
                }
            }
        });
    }

    public class AddRunnable implements Runnable {
        public void run() {

            //do some tasks
            if(p != null){
                adapter.addPlayer(p);
                p = null;
            }

            synchronized (this) {
                this.notify();
            }
        }
    }

    public class RemoveRunnable implements Runnable {
        public void run() {

            //do some tasks
            if(p != null){
                adapter.removePlayer(p);
                p = null;
            }

            synchronized (this) {
                this.notify();
            }
        }
    }

    @Override
    public void onBackPressed() {
        leaveRoom();
        super.onBackPressed();
    }

    private void leaveRoom(){
        try {
            JSONObject jsonData = new JSONObject();
            jsonData.put("room", roomNumber);
            jsonData.put("nickname", user.getNickname());
            MainActivity.mSocket.emit("leave_room", jsonData);
        } catch (Exception e){
            Log.d("socket","error calling leave_room");
        }
    }

    private void errorConnLeaveListener() {
        MainActivity.mSocket.on("connection_error_leave", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.d("socket","connection_error_leave reached");
                leaveRoom();
            }
        });
    }

}
