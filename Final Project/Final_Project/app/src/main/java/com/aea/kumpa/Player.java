package com.aea.kumpa;

import android.os.Parcel;
import android.os.Parcelable;

public class Player implements Parcelable {
    private String nickname;
    private int avatar;
    private int creator;
    private int score;
    private int place;

    public int getPlace(){
        return place;
    }

    public void setPlace(int place){
        this.place = place;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAvatar() {
        return avatar;
    }

    public void setAvatar(int avatar) {
        this.avatar = avatar;
    }

    public int getCreator() {
        return creator;
    }

    public void setCreator(int creator) {
        this.creator = creator;
    }

    public Player(){

    }

    public Player(String nickname, int avatar, int creator){
        this.avatar = avatar;
        this.creator = creator;
        this.nickname = nickname;
        this.score = 0;
    }

    public Player(String nickname, int avatar, int creator, int score){
        this.avatar = avatar;
        this.creator = creator;
        this.nickname = nickname;
        this.score = score;
    }


    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int arg1) {
        // TODO Auto-generated method stub
        dest.writeInt(avatar);
        dest.writeInt(creator);
        dest.writeString(nickname);
        dest.writeInt(score);
    }

    public Player(Parcel in) {
        avatar = in.readInt();
        creator = in.readInt();
        nickname = in.readString();
        score = in.readInt();
    }

    public static final Parcelable.Creator<Player> CREATOR = new Parcelable.Creator<Player>() {
        public Player createFromParcel(Parcel in) {
            return new Player(in);
        }

        public Player[] newArray(int size) {
            return new Player[size];
        }
    };
}
