package com.aea.kumpa;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.socket.emitter.Emitter;

public class QuestionActivity extends AppCompatActivity {

    private TextView questionText;
    private TextView answer1;
    private TextView answer2;
    private TextView answer3;
    private TextView answer4;
    private Button next_btn;

    private Question question;

    private ArrayList<Player> players;
    private Player user;

    private ArrayList<Answer> answers;
    private Answer a;

    private Integer correct;
    private Integer current;
    private String currentNick;
    private Integer numquestions;
    private Integer roomNumber;


    private RecyclerView recyclerView1;
    private RecyclerView recyclerView2;
    private RecyclerView recyclerView3;
    private RecyclerView recyclerView4;

    private RecyclerView.LayoutManager layoutManager1;
    private RecyclerView.LayoutManager layoutManager2;
    private RecyclerView.LayoutManager layoutManager3;
    private RecyclerView.LayoutManager layoutManager4;

    private LinearLayoutManager horizontalLayout1;
    private LinearLayoutManager horizontalLayout2;
    private LinearLayoutManager horizontalLayout3;
    private LinearLayoutManager horizontalLayout4;

    private AnswerAdapter adapter1;
    private AnswerAdapter adapter2;
    private AnswerAdapter adapter3;
    private AnswerAdapter adapter4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);

        Intent intent = getIntent();
        Bundle bundle = getIntent().getExtras();
        players = bundle.getParcelableArrayList("players");
        answers = new ArrayList<>();
        answers.clear();

        Integer avatar = intent.getIntExtra("avatar", 0);
        Integer creator = intent.getIntExtra("creator", 0);
        Integer numRounds = intent.getIntExtra("numRounds", 3);
        String nickname = intent.getStringExtra("nickname");
        user = new Player(nickname,avatar,creator);
        roomNumber = intent.getIntExtra("room", 0);

        current = 0;
        numquestions = players.size()*numRounds;

        System.out.println("numRounds: " + numRounds.toString());
        System.out.println("numquestions: " + numquestions.toString());

        questionText = findViewById(R.id.questionText);
        answer1 = findViewById(R.id.answer1);
        answer2 = findViewById(R.id.answer2);
        answer3 = findViewById(R.id.answer3);
        answer4 = findViewById(R.id.answer4);



        recyclerView1 = findViewById(R.id.answers_recycler_view_1);
        layoutManager1 = new LinearLayoutManager(this);
        adapter1 = new AnswerAdapter();
        recyclerView1.setLayoutManager(layoutManager1);
        recyclerView1.setAdapter(adapter1);
        horizontalLayout1 = new LinearLayoutManager( QuestionActivity.this, LinearLayoutManager.HORIZONTAL, true);
        recyclerView1.setLayoutManager(horizontalLayout1);
        recyclerView1.setVisibility(View.INVISIBLE);

        recyclerView2 = findViewById(R.id.answers_recycler_view_2);
        layoutManager2 = new LinearLayoutManager(this);
        adapter2 = new AnswerAdapter();
        recyclerView2.setLayoutManager(layoutManager2);
        recyclerView2.setAdapter(adapter2);
        horizontalLayout2 = new LinearLayoutManager( QuestionActivity.this, LinearLayoutManager.HORIZONTAL, true);
        recyclerView2.setLayoutManager(horizontalLayout2);
        recyclerView2.setVisibility(View.INVISIBLE);

        recyclerView3 = findViewById(R.id.answers_recycler_view_3);
        layoutManager3 = new LinearLayoutManager(this);
        adapter3 = new AnswerAdapter();
        recyclerView3.setLayoutManager(layoutManager3);
        recyclerView3.setAdapter(adapter3);
        horizontalLayout3 = new LinearLayoutManager( QuestionActivity.this, LinearLayoutManager.HORIZONTAL, true);
        recyclerView3.setLayoutManager(horizontalLayout3);
        recyclerView3.setVisibility(View.INVISIBLE);

        recyclerView4 = findViewById(R.id.answers_recycler_view_4);
        layoutManager4 = new LinearLayoutManager(this);
        adapter4 = new AnswerAdapter();
        recyclerView4.setLayoutManager(layoutManager4);
        recyclerView4.setAdapter(adapter4);
        horizontalLayout4 = new LinearLayoutManager( QuestionActivity.this, LinearLayoutManager.HORIZONTAL, true);
        recyclerView4.setLayoutManager(horizontalLayout4);
        recyclerView4.setVisibility(View.INVISIBLE);

        next_btn = findViewById(R.id.next_btn);
        next_btn.setVisibility(View.INVISIBLE);
        next_btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                nextBtn();
            }
        });

        nextQuestion();

        nextQuestionListener();
        answerQuestionListener();
        endGameListener();

        errorConnNextListener();
        errorConnAnswerListener();
        errorConnEndListener();
    }

    private boolean endQuestion(){
        return answers.size() == players.size();
    }

    private void errorConnNextListener() {
        MainActivity.mSocket.on("connection_error_next", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.d("socket","connection_error_next reached");
                nextQuestion();
            }
        });
    }

    private void nextQuestion(){
        if(user.getCreator() == 1){
            currentNick = players.get(current).getNickname();
            Log.d("currentNick",currentNick);
            try {
                question = chooseNextQuestion();
                Log.d("question", question.toString());
                JSONObject jsonData = new JSONObject();
                jsonData.put("room", roomNumber);
                jsonData.put("idQuestion", question.getId());
                jsonData.put("currentNick", currentNick);
                MainActivity.mSocket.emit("next_question", jsonData);
            } catch (Exception e){
                e.printStackTrace();
                Log.d("socket","error calling next_question");
            }
        }


    }

    private void nextQuestionListener(){
        MainActivity.mSocket.on("next", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.d("socket","next reached");
                answers.clear();
                try {
                    JSONObject questionJson = new JSONObject(args[0].toString());
                    Integer q = questionJson.getInt("idQuestion");
                    currentNick = questionJson.getString("currentNick");
                    question = MainActivity.database.questionDao().getById(q);
                    Integer n = question.getN_times();
                    n++;
                    MainActivity.database.questionDao().update(n,1, q);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("socket","error calling next");
                }

                replaceText();
                current = (current+1) % players.size();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(numquestions > 0){

                            recyclerView1.setVisibility(View.INVISIBLE);
                            recyclerView2.setVisibility(View.INVISIBLE);
                            recyclerView3.setVisibility(View.INVISIBLE);
                            recyclerView4.setVisibility(View.INVISIBLE);

                            //clear answers
                            adapter1.resetAdapter();
                            adapter2.resetAdapter();
                            adapter3.resetAdapter();
                            adapter4.resetAdapter();

                            unblockButtons();

                            answer1.setBackground(getDrawable(R.drawable.light_blue_background));
                            answer2.setBackground(getDrawable(R.drawable.light_blue_background));
                            answer3.setBackground(getDrawable(R.drawable.light_blue_background));
                            answer4.setBackground(getDrawable(R.drawable.light_blue_background));

                            questionText.setText(question.getQuestion_text());
                            answer1.setText(question.getAnswer1());
                            answer2.setText(question.getAnswer2());
                            answer3.setText(question.getAnswer3());
                            answer4.setText(question.getAnswer4());

                            correct = -1;
                            numquestions--;
                        }else{
                            finalScreen();
                        }
                    }
                });
            }
        });
    }

    private Question chooseNextQuestion(){
        Question q = MainActivity.database.questionDao().chooseQuestion();
        return q;
    }

    private void finalScreen(){
        MainActivity.database.questionDao().endGame();

        /*CharSequence text = "Esta deberia ser la final screen pero flojera codearla";
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();*/
        //emit end game
        if(user.getCreator()==1){
            try {
                JSONObject jsonData = new JSONObject();
                jsonData.put("room", roomNumber);
                MainActivity.mSocket.emit("end_game", jsonData);
            } catch (Exception e){
                Log.d("socket","error calling end_game");
            }
        }
    }

    private void errorConnAnswerListener() {
        MainActivity.mSocket.on("connection_error_answer", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                try {
                    JSONObject answerJson = new JSONObject(args[0].toString());
                    Integer answer = answerJson.getInt("answer");
                    answerQuestion(answer);

                } catch (JSONException e) {
                    Log.d("socket","error parsing Json on connection_error_answer");
                    e.printStackTrace();
                }
            }
        });
    }

    private void answerQuestion(int n){
        try {
            JSONObject jsonData = new JSONObject();
            jsonData.put("room", roomNumber);
            jsonData.put("nickname", user.getNickname());
            jsonData.put("answer", n);
            jsonData.put("avatar", user.getAvatar());
            MainActivity.mSocket.emit("answer", jsonData);
            blockButtons();
        } catch (Exception e){
            Log.d("socket","error calling answer");
        }
    }

    private void answerQuestionListener(){
        MainActivity.mSocket.on("player_answer", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.d("socket","player_answer reached");
                JSONObject answerJson = null;
                a = null;
                try {
                    answerJson = new JSONObject(args[0].toString());
                    String nickname = answerJson.getString("nickname");
                    Integer avatar = answerJson.getInt("avatar");
                    Integer answer = answerJson.getInt("answer");
                    a = new Answer(answer, nickname, avatar);

                    if(nickname.equals(currentNick)){
                        correct = a.getAnswer();
                    }

                } catch (JSONException e) {
                    Log.d("socket","error parsing Json on player_answer");
                    e.printStackTrace();
                }

                answers.add(a);
                a = null;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(endQuestion()){
                            showResults();
                        }
                    }
                });
            }
        });
    }

    private void showResults(){
        //increase players scores
        int n = answers.size();
        for (int i = 0; i < n; i++){
            Answer ans = answers.get(i);
            if(ans.getAnswer() == correct){
                increase_score(ans.getNickname());
            }
        }

        //recycleviews with avatars appear
        for (int i = 0; i < n; i++){
            Answer ans = answers.get(i);
            if(ans.getNickname().equals(currentNick)){
            }
            else if(ans.getAnswer() == 1){
                adapter1.addAnswer(ans);
            }else if(ans.getAnswer() == 2){
                adapter2.addAnswer(ans);
            }else if(ans.getAnswer() == 3){
                adapter3.addAnswer(ans);
            }else if(ans.getAnswer() == 4){
                adapter4.addAnswer(ans);
            }
        }


        recyclerView1.setVisibility(View.VISIBLE);
        recyclerView2.setVisibility(View.VISIBLE);
        recyclerView3.setVisibility(View.VISIBLE);
        recyclerView4.setVisibility(View.VISIBLE);

        //correct answer change background
        if(correct == 1){
            answer1.setBackground(getDrawable(R.drawable.light_green_background));
            answer2.setBackground(getDrawable(R.drawable.red_background));
            answer3.setBackground(getDrawable(R.drawable.red_background));
            answer4.setBackground(getDrawable(R.drawable.red_background));
        }else if (correct == 2){
            answer1.setBackground(getDrawable(R.drawable.red_background));
            answer2.setBackground(getDrawable(R.drawable.light_green_background));
            answer3.setBackground(getDrawable(R.drawable.red_background));
            answer4.setBackground(getDrawable(R.drawable.red_background));
        }else if (correct == 3){
            answer1.setBackground(getDrawable(R.drawable.red_background));
            answer2.setBackground(getDrawable(R.drawable.red_background));
            answer3.setBackground(getDrawable(R.drawable.light_green_background));
            answer4.setBackground(getDrawable(R.drawable.red_background));
        }else if (correct == 4){
            answer1.setBackground(getDrawable(R.drawable.red_background));
            answer2.setBackground(getDrawable(R.drawable.red_background));
            answer3.setBackground(getDrawable(R.drawable.red_background));
            answer4.setBackground(getDrawable(R.drawable.light_green_background));
        }

        //show next btn
        if(user.getCreator()==1){
            next_btn.setVisibility(View.VISIBLE);
        }
    }

    private void  nextBtn(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                //call next question
                nextQuestion();
            }
        });
    }


    private void endGameListener(){
        MainActivity.mSocket.on("game_ended", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.d("socket","game_ended reached");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Context context = getApplicationContext();
                        Intent intent = new Intent(context, ScoresActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("room", roomNumber);
                        intent.putExtra("avatar", user.getAvatar());
                        intent.putExtra("creator", user.getCreator());
                        intent.putExtra("nickname", user.getNickname());
                        Bundle bundle = new Bundle();
                        bundle.putParcelableArrayList("players", players);
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                    }
                });
            }
        });
    }



    private void errorConnEndListener() {
        MainActivity.mSocket.on("connection_error_end", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.d("socket","connection_error_end reached");
                finalScreen();
            }
        });
    }


    private void increase_score(String nickname){

        if(nickname.equals(currentNick)){
            return;
        }

        int n = players.size();
        for (int i = 0; i < n; i++){
            Player play = players.get(i);
            if(play.getNickname().equals(nickname)){
                Integer s = play.getScore();
                s ++;
                play.setScore(s);
                players.set(i,play);
                return;
            }
        }
    }

    public void selectOption1(View v){
        answerQuestion(1);
        answer1.setBackground(getDrawable(R.drawable.orange_background));
    }

    public void selectOption2(View v){
        answerQuestion(2);
        answer2.setBackground(getDrawable(R.drawable.orange_background));
    }

    public void selectOption3(View v){
        answerQuestion(3);
        answer3.setBackground(getDrawable(R.drawable.orange_background));
    }

    public void selectOption4(View v){
        answerQuestion(4);
        answer4.setBackground(getDrawable(R.drawable.orange_background));
    }

    private void replaceText(){
        String s = question.getQuestion_text();
        String temp = s.replace("_nick_", currentNick);
        question.setQuestion_text(temp);
    }

    private void blockButtons(){
        answer1.setEnabled(false);
        answer2.setEnabled(false);
        answer3.setEnabled(false);
        answer4.setEnabled(false);
    }

    private void unblockButtons(){
        answer1.setEnabled(true);
        answer2.setEnabled(true);
        answer3.setEnabled(true);
        answer4.setEnabled(true);
    }

    @Override
    public void onBackPressed() {

    }
}