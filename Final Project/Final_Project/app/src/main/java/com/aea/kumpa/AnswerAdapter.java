package com.aea.kumpa;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class AnswerAdapter extends RecyclerView.Adapter<AnswerAdapter.AnswerViewHolder> {
    public class AnswerViewHolder extends RecyclerView.ViewHolder{
        public ConstraintLayout containerView;
        public ImageView answerAvatarImg;
        public TextView answerNickText;

        public AnswerViewHolder(View view) {
            super(view);
            this.containerView = view.findViewById(R.id.answer_row);
            this.answerAvatarImg = view.findViewById(R.id.answerAvatarImg);
            this.answerNickText = view.findViewById(R.id.answerNickText);
        }


    }

    private List<Answer> answers = new ArrayList<>();

    @Override
    public AnswerAdapter.AnswerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.answer_row, parent, false);

        return new AnswerAdapter.AnswerViewHolder(view);
    }

    public static int getImageId(Context context, Integer imageName) {
        return context.getResources().getIdentifier("drawable/a" + imageName.toString(), null, context.getPackageName());
    }

    @Override
    public void onBindViewHolder(AnswerAdapter.AnswerViewHolder holder, int position) {
        Answer current = answers.get(position);
        holder.containerView.setTag(current);

        //holder.playerAvatarImg.setImageResource(R.drawable.a1);
        holder.answerAvatarImg.setImageResource(getImageId(holder.containerView.getContext(), current.getAvatar()));
        holder.answerNickText.setText(current.getNickname().substring(0,1));
    }

    @Override
    public int getItemCount() {
        return answers.size();
    }

    public void resetAdapter(){
        answers.clear();
        notifyDataSetChanged();
    }

    public void addAnswer(Answer a) {
        answers.add(a);
        notifyDataSetChanged();
        return;
    }
}
