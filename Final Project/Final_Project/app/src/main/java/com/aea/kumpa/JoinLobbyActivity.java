package com.aea.kumpa;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.socket.emitter.Emitter;

public class JoinLobbyActivity extends AppCompatActivity {

    private EditText editText;
    private ImageView avatarImg;
    private Button joinBtn;
    private int code;
    private String nickname;
    private Integer error_code;
    private String error_text;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join_lobby);

        Intent intent = getIntent();
        editText = findViewById(R.id.joinInput);

        int avatar = intent.getIntExtra("avatar", 0);
        nickname = intent.getStringExtra("nickname");
        avatarImg = findViewById(R.id.avatarImgJoin);
        avatarImg.setImageResource(getImageId(getApplicationContext(), avatar));

        joinBtn = findViewById(R.id.enterBtn);
        joinBtn.setEnabled(true);


        joinBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                joinBtn.setEnabled(false);
                validate();
            }
        });

        validateListener();
    }

    public static int getImageId(Context context, Integer imageName) {
        return context.getResources().getIdentifier("drawable/a" + imageName.toString(), null, context.getPackageName());
    }

    private void validate(){
        try {
            if(editText.getText().toString().matches("^[1-9]\\d*$")){
                Integer room = Integer.parseInt(editText.getText().toString());
                JSONObject jsonData = new JSONObject();
                jsonData.put("room", room);
                jsonData.put("nickname", nickname);
                MainActivity.mSocket.emit("validate", jsonData);
            }else{
                Context context = getApplicationContext();
                CharSequence text = "Room code must be a number";
                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
                return;
            }
        } catch (Exception e){
            Log.d("socket","error calling join room");
        }

    }

    private void validateListener(){
        MainActivity.mSocket.on("error_validate", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                try {
                    JSONObject errorJson = new JSONObject(args[0].toString());
                    error_code = errorJson.getInt("id");
                    error_text = errorJson.getString("errorText");


                } catch (JSONException e) {
                    Log.d("socket","error parsing Json on new_player");
                    e.printStackTrace();
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        joinBtn.setEnabled(true);
                        if (error_code >= 0){
                            Context context = getApplicationContext();
                            CharSequence text = error_text;
                            int duration = Toast.LENGTH_SHORT;
                            Toast toast = Toast.makeText(context, text, duration);
                            toast.show();
                            return;
                        }

                        joinBtn.setEnabled(true);
                        Context context = getApplicationContext();
                        Intent intent_read = getIntent();
                        Intent intent = new Intent(context, LobbyActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("nickname", intent_read.getStringExtra("nickname"));
                        intent.putExtra("creator", 0);
                        intent.putExtra("avatar", intent_read.getIntExtra("avatar", 0));
                        intent.putExtra("room", Integer.parseInt(editText.getText().toString()));
                        intent.putExtra("firstGame", true);
                        context.startActivity(intent);
                    }
                });
                //JSONObject messageJson = new JSONObject(args[0].toString());
                //String room = messageJson.getString("message");
            }
        });
    }
}
