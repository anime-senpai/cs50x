package com.aea.kumpa;

public class Answer {
    private int answer;
    private int avatar;
    private String nickname;


    public int getAnswer() {
        return answer;
    }

    public void setAnswer(int answer) {
        this.answer = answer;
    }

    public int getAvatar() {
        return avatar;
    }

    public void setAvatar(int avatar) {
        this.avatar = avatar;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Answer(){

    }

    public Answer(int answer, String nickname, int avatar){
        this.answer = answer;
        this.nickname = nickname;
        this.avatar = avatar;
    }
}
