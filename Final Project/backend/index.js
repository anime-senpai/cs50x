





var TIME_LIMIT = 10; //10 seconds before random number
var SLEEP_TIME = 5; // 5 seconds for send reward before start next turn

function User(id, money, betValue)
{
    this.id = id; // Keep connection id (Socket id)
    this.money = money; // keep user money
    this.betValue = betValue; // Keep user bet value
}

var ArrayList = require('arraylist')

var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

// Create a List of User playing
var listUsers = new ArrayList;

var money = 0; // TOTAL money bet on server

//Create SERVER
app.get('/', function(req,res){

    res.sendFile('index.html',{root: __dirname }) // when user use GET method, return an default index

});

function getRandomInt(max){
    return Math.floor(Math.random()*Math.floor(max)); // Create random int from 0 to max
}

function sleep(sec)
{
    return new Promise(resolve => setTimeout(resolve,sec*1000)); // Sleep
}

async function countDown() // Broad cast count down timer for all client
{

    var timeTotal = TIME_LIMIT;
    do{

        //Send tomer to all clients
        io.sockets.emit('broadcast', timeTotal);
        timeTotal--;
        await sleep(1); // Sleep 1 seconds

    }while(timeTotal > 0);

    //After time limit have finished
    processResult(); // Send rewards money for winner

    //Reset data for next turn
    timeTotal = TIME_LIMIT;
    money = 0;
    io.sockets.emit('wait_before_restart', SLEEP_TIME); // Send message wait server calculate result before next turn
    io.sockets.emit('money_send',0); // Send total of money to all user (next turn default is 0)
    await sleep(SLEEP_TIME); // Wait

    io.sockets.emit('restart', 1); // Send message next turn for all clients (don't care about 1 :D you can use any numbre kaka)

    countDown();

}

function processResult()
{
    console.log('Server is processing data');
    var result = getRandomInt(2); // Will generate from 0 to 1
    console.log('Lucky Number: ' + result);
    io.sockets.emit('result', result); // Send lucky number to all clients

    //Because we only accept 1 times bet in turn, so we will remove all duplicated data
    //Of course, in client app, we will prevent this case, don't worry

    listUsers.unique();

    //Count in list User playing how many winners
    var count = listUsers.find(function(user){
        return user.betValue == result;
    }).lenght;

    //Now just find winner and loser to send reward
    listUsers.find(function(user){
        if(user.betValue == result)
            io.to(user.id).emit('reward', parseInt(user.money)*2) // We will multiple money bet of user for reward
        else
            io.to(user.id).emit('lose',user.money);
    });

    console.log('We have ' + count +' people(s) who won');

    //Clear list players
    listUsers.clear();
}

//Process Connection Socket
io.on('connection', function(socket){

    console.log('A new user ' + socket.id + ' has connected');

    io.sockets.emit('money_send', money); // As soon as user logged on Server, we will send sum of money of this turn to him

    socket.on('client_send_money', function(objectClient)
    {
        //When user place a bet, we will get money and increase our total money
        console.log(objectClient); // Print objectClient (json format)
        var user = new User(socket.id, objectClient.money, objectClient.betValue);

        console.log('We receive ' + user.money + ' from ' + user.id);
        console.log('User ' + user.id + ' bet value ' + user.betValue);

        money += parseInt(user.money);
        
        console.log('Sum of money: ' + money); //Update on our server

        //Save user to list user online
        listUsers.add(user);
        console.log('Total online users: ' + listUsers.lenght);

        //Send update money to all user
        io.sockets.emit('money_send', money);
    });

    
    //Whenever someone disconnects
    socket.on('disconnect', function(){
    
        console.log('User ' + socket.id + ' has left');
    });

});

//Start Server
http.listen(3000,function(){

    console.log('SERVER GAME STARTED ON PORT: 3000')

    countDown();

});
